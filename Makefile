#!/usr/bin/make -f
SVC_NAME:= cowsay

REGISTRY_BASE_URL:= 'registry.gitlab.com'
DOCKER_REPO:= $(REGISTRY_BASE_URL)/tikal-external/academy-public/images

IMAGE_NAME := $(DOCKER_REPO)/$(SVC_NAME)

.PHONY: build run stop run-clean clean

export DOCKER_BUILDKIT=0

build:
	@docker build -t $(IMAGE_NAME) .

clean-prev-run:
	@docker rm -f $(SVC_NAME)_local 2>/dev/null

run: clean-prev-run 
	@docker run --rm --name $(SVC_NAME)_local $(IMAGE_NAME) -f tux "AWESOME! local test"

stop:
	docker stop $(SVC_NAME)_local

login:
	docker login $(REGISTRY_BASE_URL)

push: login
	docker push $(IMAGE_NAME)