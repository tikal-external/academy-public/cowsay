FROM ubuntu:latest

LABEL maintainer="Haggai Philip Zagury <hagzag@tikalk.com>"

# install cowsay, and move the "default.cow" out of the way so we can overwrite it with "docker.cow"
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt update && \
    apt install -y cowsay && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/games/cowsay"]
